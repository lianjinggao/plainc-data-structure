#ifndef GLLINKEDLIST_H
#define GLLINKEDLIST_H
/*
    Copy right xunshuairu Gao Lianjing.
    Only myself can use this code, modify this code, compile this code, use this code.
    All right reserved!

*/
#include "types.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// debug header
#include "GLDebug.h"
/*
    In fact using this kind of LinkedList we can store different types of data into this structure, by changing a little bit of codes
    It will be the same as the Objective-C style which can store different types of data in the homogeneous data structure either.

*/

typedef struct GLLinkNode
{
//      unsigned long key;
    struct GLLinkNode* next;
    struct GLLinkNode* prev;
    void* content;
    //void* reserve;
} GLLinkNode;


typedef struct GLLinkedList
{
    GLLinkNode* header;
    GLLinkNode* tail;

    // maintian vaiables
    int element_Size;
    unsigned long capacity;

    // functions here
    //functions of GLLinkedList
    BOOL (*init)(struct GLLinkedList*, int);
    BOOL (*destory)(struct GLLinkedList*);

    //getting methods
    void* (*get)(struct GLLinkedList*,unsigned long);
    void* (*getFirst)(struct GLLinkedList*);
    void* (*getLast) (struct GLLinkedList*);
    unsigned long (*indexOf)(struct GLLinkedList*,void*);
    unsigned long (*lastIndexOf)(struct GLLinkedList*,void*);

    BOOL (*add)(struct GLLinkedList*,void*);
    BOOL (*addFirst)(struct GLLinkedList*,void*);
    BOOL (*addInsert)(struct GLLinkedList*, void* element, unsigned long index);// Insert is not replace!
    BOOL (*addAll)(struct GLLinkedList*, struct GLLinkedList* clist2);
    BOOL (*addRang)(struct GLLinkedList*,struct GLLinkedList*,unsigned long, unsigned long);
    //deleting method
    void (*runClear)(struct GLLinkedList*);//It didn't change the type the data structure can store;
    BOOL (*removeIndex)(struct GLLinkedList*, unsigned long);//
    BOOL (*removeElement)(struct GLLinkedList*, void*);//remove the element's first apperance;
    BOOL (*removeRang)(struct GLLinkedList*,unsigned long,unsigned long);

    struct GLLinkedList (*merg)(struct GLLinkedList*, struct GLLinkedList*);
    BOOL (*divide)(struct GLLinkedList*, struct GLLinkedList*, unsigned long);

    BOOL (*contain)(struct GLLinkedList*, void*);// This method is a kind of "hard compare". It's not only compare the address, but also the memory.
    BOOL (*isEmpty)(struct GLLinkedList*);
    unsigned long (*getSize)(struct GLLinkedList*);
    //replace method
    BOOL (*setReplace)(struct GLLinkedList*, void*, unsigned long);


    //copy method

    struct GLLinkedList (*deepcopy)(struct GLLinkedList*);
    struct GLLinkedList (*clone)(struct GLLinkedList*);
    struct GLLinkedList (*copyRang)(struct GLLinkedList*,unsigned long, unsigned long);

} GLLinkedList;

// initial and delete method
BOOL GLLinkedList_alloc_space_for_data(struct GLLinkedList* clist, int elementSize);// In fact it do nothing but initilize the value of the GLinkedList
BOOL GLLinkedList_space_free(struct GLLinkedList* clist);//done


// getting method// gettign method is  getting content of node
void * GLLinkedList_get(struct GLLinkedList* clist, unsigned long index);//done and tested
void * GLLinkedList_get_first(struct GLLinkedList* clist);//done
void * GLLinkedList_get_last(struct GLLinkedList* clist);//done
unsigned long GLLinkedList_indexof(struct GLLinkedList* clist, void* element);// return the first appearance of the element's index
unsigned long GLLinkedList_lastIndexOf(struct GLLinkedList* clist, void* element);
// adding method
BOOL GLLinkedList_add_last(struct GLLinkedList*  clist,void* element);//full and tested
BOOL GLLinkedList_add_first(struct GLLinkedList* clist,void* element);//done
BOOL GLLinkedList_add_insert(struct GLLinkedList* clist,void* element, unsigned long index);//done
BOOL GLLinkedList_add_all(struct GLLinkedList* clist, struct GLLinkedList* clist2);// clist2 will behind clist
BOOL GLLinkedList_add_rang(struct GLLinkedList* clist, struct GLLinkedList* clist2, unsigned long from, unsigned long to);
//deleting method
void GLLinkedList_clear(struct GLLinkedList* clist);//It didn't change the type the data structure can store;
BOOL GLLinkedList_remove_by_index(struct GLLinkedList* clist, unsigned long index);//done
BOOL GLLinkedList_remove_by_element(struct GLLinkedList* clist, void * element);//remove the element's first apperance;
BOOL GLLinkedList_removeRang(struct GLLinkedList* clist, unsigned long from, unsigned long to);//done
// copying method
GLLinkedList GLLinkedList_clone(struct GLLinkedList* clist);//shallow copy done
GLLinkedList GLLinkedList_copy(struct GLLinkedList* clist);// deep copy done
GLLinkedList GLLinkedList_copy_Range(struct GLLinkedList* clist, unsigned long from, unsigned long to);// need to write

//merge method
GLLinkedList GLLinkedList_merge(struct GLLinkedList* clist, struct GLLinkedList* clist2);//merge is differet with the add_all. It will create a new list and return it.
BOOL GLLinkedList_divide(struct GLLinkedList* clist, struct GLLinkedList* clist2, unsigned long dividePoint);//divide the clist into two part clist and clist2 from the dividePoint.

//checking method
BOOL GLLinkedList_contain(struct GLLinkedList* clist, void* element);//done
BOOL GLLinkedList_isEmpty(struct GLLinkedList* clist);//done
unsigned long GLLinkedList_size(struct GLLinkedList* clist);//done


//replace method
BOOL GLLinkedList_set(struct GLLinkedList* clist, void* element, unsigned long index);//done


//Inner methods
BOOL GLLinkedList_test_Index(unsigned long capacity, unsigned long index);// It's testing the index's correctness
void GLLinkedList_destory_node(GLLinkNode* node);     // It's for deleteing a node
GLLinkNode* GLLinkedList_copy_a_node(GLLinkNode* node, int elementSize);// It's for copying a single node
void GLLinkedList_check_node(GLLinkNode* node); // print the information of a node
BOOL GLLinkedList_test_Index_In_List(struct GLLinkedList* clist,unsigned long from, unsigned long to);
BOOL GLLinkedList_compare_node_with_element(GLLinkNode* node, void* element, int elementSize);


//Inner checking methods
int GLLinkedList_properties_legal(GLLinkedList* clist);
void GLLinkedList_print_node(GLLinkNode* node);
void GLLinkedList_print_list(GLLinkedList* clist);

#endif // GLLINKEDLIST_H
