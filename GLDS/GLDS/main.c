#include <stdio.h>
#include <stdlib.h>

//

#include "EnvInit.h"
//
// Must define the size for the static memory section
int main()
{
    printf(" int  %lu\n", sizeof(int));
    
    EnvStart();// This is needed to start the environment of the framework.
    int d1 = 1;
    int d2 = 2;
    int d3 = 3;
    int d4 = 4;
    int d5 = 5;
    int d6 = 6;
    
    GLLinkedList newList= alloc_GLLinkedList();
    newList.init(&newList,sizeof(int));
    GLAdd(newList,d1);
    GLAdd(newList,d2);
    GLAdd(newList,d3);
    GLAdd(newList,d4);
    GLAdd(newList,d5);
    GLAdd(newList,d6);
    GLLinkedList newList3 = newList.copyRang(&newList,0,5);
    
    GLLinkedList newList4= alloc_GLLinkedList();
    newList4.init(&newList4,sizeof(int));
    
    printf(" This testing result is --> %d", GLLinkedList_properties_legal(&newList3));
    
    printf("\n%d %d %d %d %d %d",
           fetch(int,newList3.get(&newList3,0)),
           fetch(int,newList3.get(&newList3,1)),
           fetch(int,newList3.get(&newList3,2)),
           fetch(int,newList3.get(&newList3,3)),
           fetch(int,newList3.get(&newList3,4)),
           fetch(int,newList3.get(&newList3,5))
           );
    GLLinkedList_print_list(&newList3);
    printf("\nThe end");
    newList3.removeIndex(&newList3,5);
    GLLinkedList_print_list(&newList3);
    printf("\nThe end");
    
    return 0;
}
