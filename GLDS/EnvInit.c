#include "EnvInit.h"
GLArray p_GLArray_20130226;
GLLinkedList p_GLLinkedList_20130320;

/*test here*/

 //Array to copy declared here



/* The main drawback of the


*/
GLArray alloc_GLArray(){

    return p_GLArray_20130226;// Write in this way to avoid free nonheap-object warming.
}

GLLinkedList alloc_GLLinkedList(){
    return p_GLLinkedList_20130320;
}

// Here is the Environment initialization
BOOL EnvStart(){
    printf( "EnvInitStart\n" );
    prototype_GLArray();
    prototype_GLLinkedList();
    return true;
}
//

BOOL prototype_GLArray(){


    p_GLArray_20130226.init=GLArray_alloc_space_for_data;
    p_GLArray_20130226.destory = GLArray_space_free;
    p_GLArray_20130226.add = GLArray_add;
    p_GLArray_20130226.get=GLArray_get;
    p_GLArray_20130226.addInsert=GLArray_add_insert;
    p_GLArray_20130226.setReplace=GLArray_set;
    p_GLArray_20130226.isEmpty=GLArray_isEmpty;
    p_GLArray_20130226.contain=GLArray_contain;
    p_GLArray_20130226.getSize=GLArray_size;
    p_GLArray_20130226.lastIndexOf=GLArray_lastIndexOf;
    p_GLArray_20130226.clone = GLArray_clone;
    p_GLArray_20130226.runClear = GLArray_clear;
    p_GLArray_20130226.removeIndex=GLArray_remove_by_index;
    p_GLArray_20130226.indexOf=GLArray_indexOf;
    p_GLArray_20130226.removeElement=GLArray_remove_by_element;
    p_GLArray_20130226.removeRange = GLArray_removeRang;
    p_GLArray_20130226.increaseTo = GLArray_increase_space;
    p_GLArray_20130226.trimToSize = GLArray_trimToSize;
    return true;
}

BOOL prototype_GLLinkedList(){

    p_GLLinkedList_20130320.init    = GLLinkedList_alloc_space_for_data;
    p_GLLinkedList_20130320.destory = GLLinkedList_space_free;

    p_GLLinkedList_20130320.get     = GLLinkedList_get;
    p_GLLinkedList_20130320.getFirst = GLLinkedList_get_first;
    p_GLLinkedList_20130320.getLast = GLLinkedList_get_last;
    p_GLLinkedList_20130320.indexOf = GLLinkedList_indexof;
    p_GLLinkedList_20130320.lastIndexOf= GLLinkedList_lastIndexOf;


    p_GLLinkedList_20130320.addFirst= GLLinkedList_add_first;
    p_GLLinkedList_20130320.add     = GLLinkedList_add_last;// add is add last.
    p_GLLinkedList_20130320.addInsert = GLLinkedList_add_insert;
    p_GLLinkedList_20130320.addRang = GLLinkedList_add_rang;
    p_GLLinkedList_20130320.addAll = GLLinkedList_add_all;

    p_GLLinkedList_20130320.runClear = GLLinkedList_clear;
    p_GLLinkedList_20130320.removeIndex = GLLinkedList_remove_by_index;
    p_GLLinkedList_20130320.removeElement = GLLinkedList_remove_by_element;
    p_GLLinkedList_20130320.removeRang = GLLinkedList_removeRang;



    p_GLLinkedList_20130320.merg = GLLinkedList_merge;
    p_GLLinkedList_20130320.divide = GLLinkedList_divide;


    p_GLLinkedList_20130320.setReplace = GLLinkedList_set;


    p_GLLinkedList_20130320.deepcopy = GLLinkedList_copy;
    p_GLLinkedList_20130320.clone = GLLinkedList_clone;
    p_GLLinkedList_20130320.copyRang = GLLinkedList_copy_Range;


    return true;
}
