#ifndef EnvInit_h
#define EnvInit_h

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "types.h"
#include "GLArray.h"
#include "GLLinkedList.h"
#include "GLVector.h"


#define fetch(x,y) *((x*)y)//This is getting method

/* following is simplified adding method*/

#define GLAdd(x,y) x.add(&x,&y)

#define GLfree(x) x.destory(&x);free(&x); //It is a kind of deep free



BOOL EnvStart(); // Environment start


GLArray alloc_GLArray(); //Array's creation method
BOOL prototype_GLArray(); // Array's function linker


GLLinkedList alloc_GLLinkedList();
BOOL prototype_GLLinkedList();




#endif // ENVINIT_H
