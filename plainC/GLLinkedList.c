#include "GLLinkedList.h"

/*
    Inner functions begin here

*/
//<checking the index>
BOOL GLLinkedList_test_Index(unsigned long capacity, unsigned long index){
    unsigned long tailIndex = capacity-1;
    if(tailIndex <0){
        return false;
    }
    if(index <= tailIndex){
        return true;
    }
    return false;
}

BOOL GLLinkedList_test_Index_In_List(struct GLLinkedList* clist,unsigned long from,unsigned long to){
    if(from>to){
        return false;
    }
    else if(from<=0){
        return false;
    }
    else if(to<=0){
        return false;
    }
    else if(from - to + 1<1 ){
        return false;
    }
    else if(clist->capacity-1<to){
        return false;
    }
    else if(clist->capacity-1<from){
        return false;
    }
    return true;
}

void GLLinkedList_check_node(GLLinkNode* node){
    printf("node address---> %p\n", node);
    printf("node next------> %p\n", node->next);
    printf("node prev------> %p\n", node->prev);
    printf("node content---> %p\n", node->content);
}



//<destory a node>
void GLLinkedList_destory_node(GLLinkNode* node){
    free(node->content);
    free(node);
}

// inner function copying a single node
GLLinkNode* GLLinkedList_copy_a_node(GLLinkNode* node, int elementSize){
        struct GLLinkNode* newNode = malloc(sizeof(GLLinkNode));
        newNode->next = node->next;
        newNode->prev = node->prev;
        newNode->content = malloc(elementSize);
        memcpy(newNode->content,node->content,elementSize);
    return newNode;
}

BOOL GLLinkedList_compare_node_with_element(GLLinkNode* node, void* element, int elementSize){
    int result = memcmp(element, node->content, elementSize);
    if(result==0){
        return true;
    }
    return false;
}


/*

inner functions end here


*/

//creating and deleting method
BOOL GLLinkedList_alloc_space_for_data(struct GLLinkedList* clist, int elementSize){

    clist->header = 0;
    clist->tail = 0;

    clist->element_Size = elementSize;
    clist->capacity = 0;

    return true;
}

BOOL GLLinkedList_space_free(struct GLLinkedList* clist){
    GLLinkNode* tmpNode = clist->header;
    GLLinkNode* tmpNode2;
    while(tmpNode!=0){
        tmpNode2 = tmpNode->next;
        GLLinkedList_destory_node(tmpNode);
        tmpNode = tmpNode2;
    }
    return true;
}


//getting method
void * GLLinkedList_get(struct GLLinkedList* clist, unsigned long index){
    if( GLLinkedList_test_Index(clist->capacity, index)== false){
        return 0;
    }
    unsigned long tmpIndex;
    GLLinkNode* currentNode;
    for(tmpIndex =0, currentNode= clist->header;tmpIndex<index;tmpIndex++){
        currentNode = currentNode->next;
    }
    return currentNode->content;
}

void * GLLinkedList_get_first(struct GLLinkedList* clist){
    if(clist->header==0){
        return 0;
    }
    else{
        return clist->header->content;
    }
    return 0;
}


void * GLLinkedList_get_last(struct GLLinkedList* clist){
    if(clist->tail==0){
        return 0;
    }
    else{
        return  clist->tail->content;
    }
    return 0;
}

// getting the index of the first apperance of a element
unsigned long GLLinkedList_indexof(struct GLLinkedList* clist, void* element){
    if(clist->capacity == 0){
        return -1;
    }
    else{
        int counter =0;
        GLLinkNode* tmpNode = clist->header;
        while(tmpNode!=0){
            if(true==GLLinkedList_compare_node_with_element(tmpNode,element,clist->element_Size)){
                return counter;
            }
            counter++;
            tmpNode = tmpNode->next;
        }
    }
    return -1;
}

unsigned long GLLinkedList_lastIndexOf(struct GLLinkedList* clist, void* element){
    if(clist->capacity == 0){
        return -1;
    }
    else{
        int counter =clist->capacity-1;
        GLLinkNode* tmpNode = clist->tail;
        while(tmpNode!=0){
            if(true==GLLinkedList_compare_node_with_element(tmpNode,element,clist->element_Size)){
                return counter;
            }
            counter--;
            tmpNode = tmpNode->prev;
        }
    }
    return -1;
}

//adding method
BOOL GLLinkedList_add_first(struct GLLinkedList* clist, void* element){
    GLLinkNode* header = clist->header;
    GLLinkNode* tail = clist->tail;
    int Size = clist->element_Size;
    if(0== header&&0==tail){
        struct GLLinkNode* node = malloc(sizeof(GLLinkNode));
        clist->header = node;
        clist->tail = node;
        clist->header->content = malloc(Size);
        memcpy(clist->header->content,element,Size);
        node->next=0;
        node->prev=0;
        clist->capacity++;
        return true;
    }
    else{
        struct GLLinkNode* node = malloc(sizeof(GLLinkNode));
        node->content = malloc(Size);
        memcpy(node->content,element,Size);

        node->next = header;
        node->prev = 0;
        header->prev = node;
        clist->header = node;


        clist->capacity++;
        return true;
    }
    return false;
}

BOOL GLLinkedList_add_last(struct GLLinkedList* clist, void* element){
    GLLinkNode* header = clist->header;
    GLLinkNode* tail = clist->tail;
    int Size = clist->element_Size;
    if(0== header&&0==tail){
        struct GLLinkNode* node = malloc(sizeof(GLLinkNode));
        clist->header = node;
        clist->tail = node;
        clist->header->content = malloc(Size);
        memcpy(clist->header->content,element,Size);
        node->next=0;
        node->prev=0;
        clist->capacity++;
        return true;
    }
    else{
        struct GLLinkNode* node = malloc(sizeof(GLLinkNode));
        node->content = malloc(Size);
        memcpy(node->content,element,Size);

        node->prev = tail;
        node->next = 0;
        tail->next = node;
        clist->tail = node;

        clist->capacity++;
        return true;
    }
    return false;
}

BOOL GLLinkedList_add_insert(struct GLLinkedList* clist,void* element, unsigned long index){
    if( GLLinkedList_test_Index(clist->capacity, index)== false){
        return false;
    }

    unsigned long tmpIndex;
    GLLinkNode* currentNode;
    for(tmpIndex =0, currentNode= clist->header;;tmpIndex++){
        if(tmpIndex == index){

            struct GLLinkNode* newNode = malloc(sizeof(GLLinkNode));
            newNode->next=currentNode;
            newNode->prev=currentNode->prev;
            currentNode->prev = newNode;
            memcpy(newNode->content, element,clist->element_Size);
            if(currentNode==clist->header){
                clist->header = newNode;
            }

            break;
        }
        currentNode = currentNode->next;
    }
    clist->capacity++;
    return true;
}

BOOL GLLinkedList_add_all(struct GLLinkedList* clist, struct GLLinkedList* clist2){
    if(GLLinkedList_size(clist2)==0){
        return true;
    }
    else if(GLLinkedList_size(clist)==0){// If the clist is empty. We need to copy a new clist2 to clist.
        GLLinkedList newList = GLLinkedList_copy(clist2);
        *clist = newList;
        return true;
    }
    else{
        GLLinkedList tmpList = GLLinkedList_copy(clist2);
        clist->tail->next = tmpList.header;
        tmpList.header->prev = clist->tail;
        clist->tail = tmpList.tail;
        clist->capacity = clist->capacity+clist2->capacity;
    }
    return true;
}

BOOL GLLinkedList_add_rang(struct GLLinkedList* clist, struct GLLinkedList* clist2, unsigned long from, unsigned long to){
    if(GLLinkedList_test_Index_In_List(clist2,from,to)==false){
        GLDEBUG_index_refer_error();
        return false;
    }
    else if(clist->capacity ==0){
        *clist = GLLinkedList_copy_Range(clist2,from,to);
        return true;
    }
    else{
        GLLinkedList tmpList = GLLinkedList_copy_Range(clist2,from,to);
        clist->tail->next = tmpList.header;
        tmpList.header->prev = clist->tail;
        clist->tail = tmpList.tail;
        clist->capacity += tmpList.capacity;
    }
    return true;
}
// delete methods

void GLLinkedList_clear(struct GLLinkedList* clist){
    if(clist->capacity==0){
        return;
    }
    else{
        GLLinkNode* tmpNode = clist->header;
        while(tmpNode!=0){
            GLLinkNode* tmpNode2 = tmpNode->next;
            GLLinkedList_destory_node(tmpNode);
            tmpNode = tmpNode2;
        }
    }
    clist->header = 0;
    clist->tail =0;
    clist->capacity=0;
    return;
}
BOOL GLLinkedList_remove_by_index(struct GLLinkedList* clist, unsigned long index){

    if( GLLinkedList_test_Index(clist->capacity, index)== false){
        return false;
    }
    unsigned long tmpIndex;
    GLLinkNode* currentNode;
    //find the currentNode need to be deleted
    for(tmpIndex =0, currentNode= clist->header;tmpIndex<index;tmpIndex++){
        currentNode = currentNode->next;
    }
    //<task of dealing with the currentNode>
    if(currentNode==clist->header&&currentNode==clist->tail){
        clist->header=0;
        clist->tail = 0;
    }
    else if(currentNode == clist->header){
        currentNode->next->prev = 0;
        clist->header = currentNode->next;
    }
    else if(currentNode == clist->tail){
        currentNode->prev->next = 0;
        clist->tail = currentNode->prev;
    }
    else{
        currentNode->prev->next = currentNode->next;
        currentNode->next->prev = currentNode->prev;
    }
    GLLinkedList_destory_node(currentNode);
    clist->capacity --;
    return true;
}

BOOL GLLinkedList_remove_by_element(struct GLLinkedList* clist, void * element){
    unsigned long index = GLLinkedList_indexof(clist,element);
    if(index==-1){
        return false;
    }
    else{
        return GLLinkedList_remove_by_index(clist, index);
    }
    return false;
}


BOOL GLLinkedList_removeRang(struct GLLinkedList* clist, unsigned long from, unsigned long to){
    if(GLLinkedList_test_Index_In_List(clist,from,to)==false){
        return false;
    }
    else{
        GLLinkNode* gapheader;
        GLLinkNode* gaptail;
        GLLinkNode* tmpNode1 = clist->header;
        GLLinkNode* tmpNode2;
        unsigned long counter=0;
        while(tmpNode1!=0){
            if(counter==from&& counter==to){
                gapheader = tmpNode1->prev;
                gaptail = tmpNode1->next;
                tmpNode2 = tmpNode1;
                GLLinkedList_destory_node(tmpNode1);
                clist->capacity--;
            }
            else if (counter == from){
                gapheader =tmpNode1->prev;
                tmpNode2 = tmpNode1;
                GLLinkedList_destory_node(tmpNode1);
            }
            else if (counter == to){
                gaptail = tmpNode1->next;
                tmpNode2 = tmpNode1;
                GLLinkedList_destory_node(tmpNode1);
                clist->capacity--;
            }
            else if (counter >from&&counter<to){
                tmpNode2 = tmpNode1;
                GLLinkedList_destory_node(tmpNode1);
                clist->capacity--;
            }
            else{
            }
            counter++;
            tmpNode1 = tmpNode2;
        }
        // the following the to link the gapheader and the gaptail;
        if(gapheader==0&&gaptail==0){
            clist->header=0;
            clist->tail=0;
            clist->capacity=0;
        }
        else if(gapheader==0){
            clist->header = gaptail;
            gaptail->prev =0;
        }
        else if(gaptail==0){
            clist->tail = gapheader;
            gapheader->next=0;
        }
        else{
            gapheader->next = gaptail;
            gaptail->prev = gapheader;
        }
        return true;
    }
    return false;
}

//copy method
GLLinkedList GLLinkedList_clone(struct GLLinkedList* clist){
    return *clist;
}

GLLinkedList GLLinkedList_copy(struct GLLinkedList* clist){
    int elementSize = clist->element_Size;
    struct GLLinkedList newList = *clist;

    newList.element_Size = clist->element_Size;
    newList.capacity = clist->capacity;

    unsigned long tmpIndex= 1;
    GLLinkNode* tmpNode = 0;
    GLLinkNode* tmpNode2 = 0;
    for(   ;tmpIndex <= clist->capacity; tmpIndex++){
        //<tmp2 = clist's node>
        if(tmpNode2 == 0){
            tmpNode2 = clist->header;
        }
        else{
            tmpNode2 = tmpNode2->next;
        }
        struct GLLinkNode* newNode = malloc(sizeof(GLLinkNode));
        newNode->content = malloc(elementSize);
        memcpy(newNode->content, tmpNode2->content,elementSize);
        if(tmpNode==0){
            newNode->prev = 0;
            newNode->next = 0;
            newList.header = newNode;
        }
        else{
            tmpNode->next = newNode;
            newNode->prev = tmpNode;
        }
        tmpNode = newNode;
        newList.tail = newNode;
    }
    return newList;
}

GLLinkedList GLLinkedList_copy_Range(struct GLLinkedList* clist, unsigned long from, unsigned long to){
    int elementSize = clist->element_Size;//It is the size of the element in the node;
    struct GLLinkedList newList = *clist; //It is the newList need to return;

    newList.element_Size = clist->element_Size;
    newList.capacity = to-from+1;
    if(GLLinkedList_test_Index_In_List(clist, from , to)==false){
        return newList;
    }
    else{
        unsigned long counter;
        struct GLLinkNode* handle  = clist->header;// This is handle in clist
        struct GLLinkNode* handle2 = 0; // This is handle in the newList
        for(counter = 0; counter<=to ; counter++){
            GLLinkNode* newNode= GLLinkedList_copy_a_node(handle,elementSize);
            if(counter==from){
                newNode->prev = 0;
                newList.header = newNode;
            }
            else if(counter == to){
                newNode->prev = handle2;
                newNode->next = 0;
                newList.tail = newNode;
                handle2->next = newNode;
            }
            else if(counter > from){
                newNode->prev = handle2;
                handle2->next = newNode;
            }
            handle2 = newNode;
            handle = handle->next;
        }
    }
    return newList;
}

// merge and divide
GLLinkedList GLLinkedList_merge(struct GLLinkedList* clist, struct GLLinkedList* clist2){
    GLLinkedList newList;
    newList.capacity=0;
    newList.init(&newList,clist->element_Size);

    newList.addAll(&newList,clist);
    newList.addAll(&newList,clist2);
    return newList;
}
BOOL GLLinkedList_divide(struct GLLinkedList* clist, struct GLLinkedList* clist2, unsigned long dividePoint){
    if(clist2->capacity!=0){
        return false;
    }
    if(clist->capacity-1<=dividePoint){
        return false;
    }
    else{
        GLLinkNode* tmpNode1 = clist->header;
        unsigned long counter =0;
        while(counter!=dividePoint){
            counter++;
            tmpNode1 = tmpNode1->next;
        }

        clist2->header = tmpNode1->next;
        if(tmpNode1 == clist->tail){
            clist2->tail = 0;
        }
        else{
        clist2->tail = clist->tail;
        }
        clist->tail = tmpNode1;
        clist2->capacity = clist->capacity - (counter+1);
        clist->capacity = counter+1;
    }
    return false;
}


//checking method
BOOL GLLinkedList_contain(struct GLLinkedList* clist, void* element){
    if(-1==GLLinkedList_indexof(clist,element)){
        return false;
    }
    else{
        return true;
    }

}

BOOL GLLinkedList_isEmpty(struct GLLinkedList* clist){
    if(clist->capacity==0){
        return true;
    }
    else{
        return false;
    }
}

unsigned long GLLinkedList_size(struct GLLinkedList* clist){
    return clist->capacity;
}

BOOL GLLinkedList_set(struct GLLinkedList* clist,void* element, unsigned long index){
    if( GLLinkedList_test_Index(clist->capacity, index)== false){
        return false;
    }
    unsigned long tmpIndex;
    GLLinkNode* currentNode;
    for(tmpIndex =0, currentNode= clist->header;;tmpIndex++){
        if(tmpIndex == index){
            memcpy(currentNode->content, element,clist->element_Size);
            break;
        }
        currentNode = currentNode->next;
    }
    return true;
}

//Inner checking methods
int GLLinkedList_properties_legal(GLLinkedList* clist){
    int storedCapacity = clist->capacity;
    //<testing the transitivity>
    int counter =0;
    int counterback = clist->capacity-1;
    GLLinkNode* tmp = clist->header;
    GLLinkNode* tmp2 = clist->tail;
    while(tmp!=0){

        tmp = tmp->next;
        tmp2= tmp2->prev;
        if( tmp!=0&&tmp2!=0 ){
        counter++;
        counterback--;
        }
    }

    if(counter!=storedCapacity-1){
        printf("This capacity stored is %d\n ", storedCapacity);
        printf("But the counter is %d\n",counter);
        return -1;
    }
    if(counterback!=0){
        return -2;
    }
    if(tmp2!=0){
        printf("The reversed tmp2 is %p\n",tmp2);
        return -3;
    }

    return 1;
}

void GLLinkedList_print_node(GLLinkNode* node){
    printf(" node adress is %p\n", node);
    printf(" node next is   %p\n", node->next);
    printf(" node prev is   %p\n", node->prev);
    printf(" node content is%p\n", node->content);

}

void GLLinkedList_print_list(GLLinkedList* clist){
    GLLinkNode* tmp =  clist->header;
    while(tmp!=0){
        GLLinkedList_print_node(tmp);
        tmp = tmp->next;
    }
}

