#ifndef GLArray_h
#define GLArray_h
// This GLArray is foundamentally array.
// This is a unmutable array.
/* An important concept of array is that in the middle of the array there is no gap inside*/
// The store section's size is fixed.

// function names
#include "types.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// debug header
#include "GLDebug.h"

typedef struct GLArray
{
    // Entry is the data entrance of the structure
    void *Entry;

    //arguments for functions to use
    int maxcapacity;
    int capacity; //This is current capacity
    int element_size; // Because the element size in a int , so per element size cannot exceed the 2 power 16
    // functions here
    //functions of GLArray
    BOOL (*init)(struct GLArray*,int,int); // longlong is the size of the

    // getting methods
    void* (*get)(struct GLArray*,int);
    // adding methods
    BOOL (*add)(struct GLArray*,void*);
    BOOL (*addInsert)(struct GLArray* current_array,void *element,int position);
    BOOL (*setReplace)(struct GLArray* current_array, void* element, int index);
    BOOL (*isEmpty)(struct GLArray* current_array);
    BOOL (*contain)(struct GLArray* current_array, void * element);
    int  (*getSize)(struct GLArray* current_array);
    int (*lastIndexOf)(struct GLArray* current_array, void* element);
    struct GLArray (*clone)(struct GLArray* current_array);
    void (*runClear)(struct GLArray* current_array);
    BOOL (*removeIndex)(struct GLArray* current_array, int index);
    int (*indexOf)(struct GLArray* current_array, void* element);
    BOOL (*removeElement)(struct GLArray* current_array, void* element);
    BOOL (*removeRange)(struct GLArray* current_array, int from, int to);
    BOOL (*increaseTo)(struct GLArray* current_array, int newsize);
    BOOL (*trimToSize)(struct GLArray* current_array);
    BOOL (*destory)(struct GLArray* curren_array);
    //deleting methods

    //copying methods

} GLArray;



//functions of GLArray
// initial and delete methods
BOOL GLArray_alloc_space_for_data(struct GLArray* current_array,int units_amount,int unit_size);//done tested
BOOL GLArray_space_free(struct GLArray* current_array);
// structure maitaining methods
BOOL GLArray_trimToSize(struct GLArray* current_array);//concate the array the excatly size the of the array.
// This is to delete the gap in the memory of the arrange space. When a delete operation execute,we have to rerange it
BOOL GLArray_increase_space(struct GLArray* current_array, int newsize);// increase the current array to a new size

// getting methods
void* GLArray_get(struct GLArray* current_array, int index);// done tested
int  GLArray_indexOf(struct GLArray* current_array, void* element);// It will only return the first ourrence of the specified element's index


// adding methods
BOOL GLArray_add(struct GLArray* current_array,void *element); //done
BOOL GLArray_add_insert(struct GLArray* current_array,void *element,int position);//The method add insert cannot insert to any postion do not any an element, unless the postion is right behind the current last element! and it can not exceed the bound of the data structure!
BOOL GLArray_add_all(struct GLArray* current_array,struct GLArray* another_array);
BOOL GLArray_add_all_from_to(struct GLArray* current_array,struct GLArray* another_array, int from ,int to);
//deleting methods
void GLArray_clear(struct GLArray* current_array);// clear do not do anything to memory, Just set the capacity to 0;
BOOL GLArray_remove_by_index(struct GLArray* current_array, int index);//
BOOL GLArray_remove_by_element(struct GLArray* current_array, void* element);//
BOOL GLArray_removeRang(struct GLArray* current_array, int from, int to);//
//copying methods
GLArray GLArray_clone(struct GLArray* current_array);// It's a shallow copy. A deep copy is needed.


//checking methods
BOOL GLArray_contain(struct GLArray* current_array, void* element);//maybe I can use some mechanicals to test the type
BOOL GLArray_isEmpty(struct GLArray* current_array);//return true when the array is empty.
int GLArray_size(struct GLArray* current_array);//Nothing to say
int GLArray_lastIndexOf(struct GLArray* current_array, void* element);// return the last apperance of a element
// element replace methods
BOOL GLArray_set(struct GLArray* current_array, void* element, int index);//done
#endif
