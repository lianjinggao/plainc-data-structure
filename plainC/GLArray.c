#include "GLArray.h"
/* in the .c file
    I should use uncommon function name, to avoiding conflict.

*/

//////////////////////////
//inner functions       //
int GLArray_test_index(int tailIndex,int index){
    if(index>tailIndex){
        GLDEBUG_index_refer_error();
        return 0;
    }
    if(index<0){
        GLDEBUG_index_refer_error();
        return 0;
    }
    return 1;
}

//////////////////////////
BOOL GLArray_alloc_space_for_data(struct GLArray* current_array,int units_amount,int unit_size)
{
    current_array->element_size=unit_size;
    current_array->Entry=malloc(unit_size*units_amount);

    if(current_array->Entry==0){
          //  printf("system, memory warming");
        return false;
    }
    //printf("\nthe function is running %d,%d",units_amount,unit_size);
    current_array->maxcapacity=units_amount;
    printf("this is current_array %p",current_array);

    return true;// return cannot 1 or 0, must use true or false;
}

BOOL GLArray_add(struct GLArray* current_array,void *element){
    //testing whether the Array is initalized
    if(current_array->Entry==0){

        printf("A GLArray has not been initalized.");
        return false;
    }


    //testing the capacity whether it is full
    if(current_array->capacity==current_array->maxcapacity){
        GLDEBUG_index_leak();
        return false;
    }

    //<If passed ptr and capacity test, let's copy it>
        //caculate the aimAddress we need to copy to
    void * aimAddress = current_array->Entry+current_array->capacity*current_array->element_size;
    int elementSize = current_array->element_size;
        //It is copy here
    memcpy(aimAddress, element, elementSize);
    current_array->capacity++;
    printf("element added\n");
    return true;
}

BOOL GLArray_add_insert(struct GLArray* current_array,void *element,int position){
    if(position>current_array->maxcapacity)
        return false;
    if(position>current_array->capacity)// capacity is index + 1
        return false;
    if(position==current_array->capacity){
        current_array->capacity++;
    }

    memcpy(current_array->Entry+position*current_array->element_size, element, current_array->element_size);

    return true;
}

void* GLArray_get(struct GLArray* current_array, int index){
    if(index<0){
        GLDEBUG_index_refer_error();
        return 0;// This return will force the program stop in somewhere.
    }
    if(index>current_array->capacity){
        GLDEBUG_index_leak();
        return 0;
    }
    int ptr_move = (current_array->element_size)*index;
    void* result = current_array->Entry+ptr_move;
    return result;
}

BOOL GLArray_set(struct GLArray* current_array, void* element, int index){
    //<Check the index place>
    if(index > (current_array->capacity-1)){
        GLDEBUG_index_refer_error();
        return false;
    }
    //<If the replace can pass the check directly copy the element to the memory place>
    int ptr_move = (current_array->element_size)*index;
    //<Copy the element to the pointed place>
    memcpy(current_array->Entry+ptr_move, element,current_array->element_size);
    return true;
}
// checking methods
BOOL GLArray_isEmpty(struct GLArray* current_array){
    if(current_array->capacity==0){
        return true;
    }
    return false;
}

BOOL GLArray_contain(struct GLArray* current_array, void* element){
    if(current_array->capacity==0){
        return false;
    }
    int compareLength = current_array->element_size;
    void * sample = current_array->Entry;
    int itr;
    for(itr=1;itr<=current_array->capacity;itr++ ){
        if(memcmp(sample, element, compareLength)==0)
            return true;
        sample += compareLength;
    }
    return false;
}

int GLArray_size(struct GLArray* current_array){
    return current_array->capacity;
}

int GLArray_lastIndexOf(struct GLArray* current_array, void* element){
    int capacity = current_array->capacity;
    if(capacity==0){
        return -1;
    }
    int index = capacity-1;
    int compareLength = current_array->element_size;
    void * sample = current_array->Entry+index*compareLength;
    for(;index>0 ;index--){
        if(memcmp(sample, element, compareLength)==0){
            return index;
        }
        else{
            sample -= compareLength;
        }
    }
    return -1;
}

GLArray GLArray_clone(struct GLArray* current_array){
    return *current_array;
}

void GLArray_clear(struct GLArray* current_array){
    current_array->capacity=0;
}

BOOL GLArray_remove_by_index(struct GLArray* current_array, int index){
    void* startptr = current_array->Entry;
    int unitLength = current_array->element_size;
    int tailIndex = current_array->capacity-1;
    if(GLArray_test_index(tailIndex,index)){
        void* str1 = startptr+ unitLength*index;
        void* str2 = startptr+ unitLength*index+1;
        memmove(str1,str2,tailIndex-index);
        current_array->capacity--;
        return true;
    }
    return false;
}

int  GLArray_indexOf(struct GLArray* current_array, void* element){
    int capacity = current_array->capacity;
    int tailIndex = capacity-1;
    if(capacity==0){
        return -1;
    }
    int index = 0;
    int compareLength = current_array->element_size;
    void * sample = current_array->Entry;
    for(;index<=tailIndex ;index++){
        if(memcmp(sample, element, compareLength)==0){
            return index;
        }
        else{
            sample = sample + compareLength;
        }

    }
    return -1;
}

BOOL GLArray_remove_by_element(struct GLArray* current_array, void* element){

    int index = GLArray_indexOf(current_array, element);
    printf("It is the index of the element need to remove %d\n", index);

    if(index == -1){
        return false;
    }
    return GLArray_remove_by_index(current_array,index);
}

BOOL GLArray_removeRang(struct GLArray* current_array, int from, int to){
    if(GLArray_isEmpty(current_array)==true){
        return false;
    }
    int tailIndex = current_array->capacity-1;
    if(GLArray_test_index(tailIndex,from)&&GLArray_test_index(tailIndex,to)){
        if(from<=to){
            int elementSize = current_array->element_size;
            void * gapstart = current_array->Entry+from*elementSize;
            void * gapend = current_array->Entry+(to+1)*(elementSize);
            int movelength = (tailIndex- to)*elementSize;
            memmove(gapstart,gapend,movelength);

            current_array->capacity = current_array->capacity-(to - from+1);

            return true;
        }
        GLDEBUG_index_refer_error();
        return false;
    }
    GLDEBUG_index_refer_error();
    return false;
}

BOOL GLArray_increase_space(struct GLArray* current_array, int newsize){
    //<check the newsize firstly>
    if(current_array->maxcapacity>=newsize){
        return false;
    }
    //<get new space of the array>
   current_array->Entry=realloc(current_array->Entry,current_array->element_size*newsize);
    current_array->maxcapacity = newsize;
    return true;
}

BOOL GLArray_trimToSize(struct GLArray* current_array){
    int capacity = current_array->capacity;
    int maxcapacity = current_array->maxcapacity;
    if(capacity == maxcapacity){
        printf(" no need to do trim array.");
        return false;
    }
    current_array->Entry=realloc(current_array->Entry,current_array->element_size*capacity);
    current_array->maxcapacity = capacity;
    return true;

}
BOOL GLArray_space_free(struct GLArray* current_array){
    free(current_array->Entry);
    current_array->Entry = 0;
    current_array->maxcapacity = 0;
    current_array->capacity = 0;
    current_array->element_size = 0;
    return true;
}
